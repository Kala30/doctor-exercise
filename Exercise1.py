import os
import sys

# Longitude is X
# Latitude is Y

location_x = -73
location_y = 40

class doctor:
    def __init__(self, name, loc_x, loc_y):
        self.name = name
        self.loc_x = loc_x
        self.loc_y = loc_y
        # Calculate distance
        self.distance = pythag(loc_x - location_x, loc_y - location_y)
    def __str__(self):
        return self.name
    name = ""
    loc_x = 0.0
    loc_y = 0.0
    distance = 0.0

def pythag(a, b):
    c = (a ** 2) + (b ** 2)
    return c

def mergeSort(doctors):
    if len(doctors) > 1:
        # Split list in two
        mid = len(doctors) // 2
        lefthalf = doctors[:mid]
        righthalf = doctors[mid:]

        # Sort the two halves
        mergeSort(lefthalf)
        mergeSort(righthalf)

        # Merge the two halves
        l = 0 # Left half index
        r = 0 # Right half index
        i = 0 # New list index

        # When both right and left are not sorted
        while l < len(lefthalf) and r < len(righthalf):
            # If left value is less than right value
            if lefthalf[l].distance < righthalf[r].distance:
                doctors[i] = lefthalf[l] # Set current index in new list to left value
                l=l+1 # Go to next value in left list
            # If right value is less than left value
            else:
                doctors[i] = righthalf[r] # Set current index in new list to right value
                r=r+1 # Go to next value in right list
            i=i+1 # Go to next value in new list

        # When right half is completed
        while l < len(lefthalf):
            doctors[i] = lefthalf[l]
            l=l+1
            i=i+1

        # When left half is completed
        while r < len(righthalf):
            doctors[i] = righthalf[r]
            r=r+1
            i=i+1

doctors = []

# Try to open file
try:
    file = open("doctors.txt")
except IOError:
    print("[!] \"doctors.txt\" does not exist!")
    sys.exit()
except:
    print("[!] An unexpected error occurred!")
    sys.exit()

# Check if file is empty
if os.stat("doctors.txt").st_size == 0: # Check if file is 0 bytes
    print("[!] \"doctors.txt\" is empty!")
    sys.exit()

# Try to read each line
try:
    for line in file:
        if line == "\n": # Skip empty lines
            continue
        else:
            doc_name = line.split(',')[0]
            doc_loc_y = float(line.split(',')[1]) # Latitude
            doc_loc_x = float(line.split(',')[2]) # Longitude
            new_doctor = doctor(doc_name, doc_loc_x, doc_loc_y)
            doctors.append(new_doctor)
except:
    print("[!] File is formatted incorrectly!")
    sys.exit()

# Sort doctors
try:
    mergeSort(doctors)
except:
    print("[!] The doctors could not be sorted properly")
    sys.exit()

# Output
print("Longitude:", location_x)
print("Latitude:", location_y)
print("There are", len(doctors), "doctors available")
print()

# Print sorted doctors
print('%-12s%-12s' % ("Name", "Distance"))
print('%-12s%-12s' % ("-"*6, "-"*6))
for i in range(len(doctors)):
    print('%-12s%-12f' % (doctors[i].name, doctors[i].distance))

    # Specifier
    # %   Start
    # -   Conversion Flag (Left Adjusted)
    # 12  Length Modifier (12 characters)
    # s   Conversion Type (str type)
